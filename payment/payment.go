package payment

import (
	"net/http"

	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
)

type PaymentGatewayService struct {
	Transactions map[string]*Transaction
}

// InitiateTransaction initiates a new payment transaction with a request body.
func (pgs *PaymentGatewayService) InitiateTransaction(c echo.Context) error {
	var transaction Transaction
	if err := c.Bind(&transaction); err != nil {
		return c.JSON(http.StatusBadRequest, map[string]string{"error": "Invalid request payload"})
	}

	// Generate a unique transaction ID (you may use a library to generate unique IDs)
	transaction.ID = uuid.New().String()

	// Save the transaction for validation
	pgs.Transactions[transaction.ID] = &transaction

	// Return the transaction details in the response
	return c.JSON(http.StatusOK, transaction)
}

// ConfirmTransaction confirms a payment transaction.
func (pgs *PaymentGatewayService) ConfirmTransaction(c echo.Context) error {
	transactionID := c.Param("id")

	// Check if the transaction ID exists
	if transaction, ok := pgs.Transactions[transactionID]; ok {
		// Update the transaction status to "confirmed"
		transaction.Status = "confirmed"

		// Return the confirmed transaction details in the response
		return c.JSON(http.StatusOK, transaction)
	}

	// Transaction not found
	return c.JSON(http.StatusNotFound, map[string]string{"error": "Transaction not found"})
}
