package main

import (
	"example-payment/payment"
	"fmt"

	"github.com/labstack/echo/v4"
)

// PaymentGatewayService handles payment gateway API endpoints.
// type PaymentGatewayService struct {
// 	Transactions map[string]*Transaction
// }

func main() {
	// Initialize Echo
	e := echo.New()

	// Initialize the PaymentGatewayService
	paymentGateway := &payment.PaymentGatewayService{
		Transactions: make(map[string]*payment.Transaction),
	}

	// Define API endpoints
	e.POST("/initiate-transaction", paymentGateway.InitiateTransaction)
	e.POST("/confirm-transaction/:id", paymentGateway.ConfirmTransaction)

	// Start the server
	port := 8080
	fmt.Printf("Payment Gateway API listening on port %d...\n", port)
	e.Start(fmt.Sprintf(":%d", port))
}
