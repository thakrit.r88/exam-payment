# exam-payment


# run project use 
> go run main.go


# The Transaction struct and Payment struct are used to represent payment transactions and manage the transactions.
# The 'InitiateTransaction' and 'ConfirmTransaction' methods are the handlers for the API endpoints, and they use the Echo framework's context (echo.Context) to handle HTTP requests and responses.
# The server is started using e.Start. The API has two endpoints: /initiate-transaction and /confirm-transaction/:id.



# curl postman

## initiate-transaction

curl --location 'http://localhost:8080/initiate-transaction' \
--header 'Content-Type: application/json' \
--data '{
    "amount": 200.0
}'

## example response :
{
    "id": "473143b6-80ee-4f5a-aa5d-940287d5840f",
    "amount": 200,
    "status": ""
}

## confirm-transaction

curl --location --request POST 'http://localhost:8080/confirm-transaction/{id}' \
--header 'Content-Type: application/json' \
--data ''

## example response :
{
    "id": "10551241-4c31-4bc9-81d3-e8ec1bf6314f",
    "amount": 200,
    "status": "confirmed"
}
